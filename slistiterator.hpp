//////////////////////////////////////////////////////////////////////////////
/// @file slistiterator.hpp
/// @author Matt Buechler :: CS153 Section 1B
/// @brief This is the implementation file for the slistiterator class
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn SListIterator ()
/// @brief Constructs the iterator
/// @pre No iterator exists
/// @post Iterator exists
/// @param None (constructor)
/// @return None (constructor)
////////////////////////////////////////////////////////////////////////////// 
template <class generic>
SListIterator<generic>::SListIterator () : m_current (NULL)
{
}

//////////////////////////////////////////////////////////////////////////////
/// @fn SListIterator (SNode<generic> *)
/// @brief Copy constructor for iterator
/// @pre A previous iterator must exist.
/// @post Current iterator contains the same settings as the old iterator.
/// @param Existing iterator to be copied
/// @return None (constructor)
////////////////////////////////////////////////////////////////////////////// 
template <class generic>
SListIterator<generic>::SListIterator (SNode<generic> * x) : m_current (x)
{
}

//////////////////////////////////////////////////////////////////////////////
/// @fn generic operator* () const
/// @brief Returns value of contents at iterator
/// @pre Iterator and list must exist
/// @post No change
/// @param None
/// @return Returns value of data at current position of iterator
////////////////////////////////////////////////////////////////////////////// 
template <class generic>
generic SListIterator<generic>::operator* () const
{
    return m_current->data;
}



//////////////////////////////////////////////////////////////////////////////
/// @fn SListIterator operator++ ();
/// @brief Moves iterator one step along list
/// @pre List exists, iterator at abitrary point along list
/// @post List exists, iterator one more step along list
/// @param None.
/// @return Nothing
////////////////////////////////////////////////////////////////////////////// 
template <class generic>
SListIterator<generic> SListIterator<generic>::operator++ ()
{
    m_current = m_current-> forward;
    return *this;
}

//////////////////////////////////////////////////////////////////////////////
/// @fn SListIterator operator++ ();
/// @brief Moves iterator indicated number of steps along list
/// @pre List exists, iterator at abitrary point along list
/// @post List exists, iterator 'n' more steps along list
/// @param 'n' steps to take along list
/// @return Nothing
////////////////////////////////////////////////////////////////////////////// 
template <class generic>
SListIterator<generic> SListIterator<generic>::operator++ (int)
{
    return ++(*this);
}


//////////////////////////////////////////////////////////////////////////////
/// @fn bool operator== (const SListIterator &) const;
/// @brief Allows you to compare the value of data under iterators
/// @pre Iterators exist
/// @post No change.
/// @param Other iterator to compare to.
/// @return Boolean
////////////////////////////////////////////////////////////////////////////// 
template <class generic>
bool SListIterator<generic>::operator== (const SListIterator & rhs) const
{
    return m_current == rhs.m_current;
}

//////////////////////////////////////////////////////////////////////////////
/// @fn bool operator!= (const SListIterator &) const;
/// @brief Allows you to compare the value of data under iterators
/// @pre Iterators exist
/// @post No change.
/// @param Other iterator to compare to.
/// @return Boolean
////////////////////////////////////////////////////////////////////////////// 
template <class generic>
bool SListIterator<generic>::operator!= (const SListIterator & rhs) const
{
    return m_current != rhs.m_current;
}
