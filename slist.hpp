//////////////////////////////////////////////////////////////////////////////
/// @file slist.hpp
/// @author David Norton :: CS153 Section 1B
/// @brief This is the implementation file for the SList class
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn SList ()
/// @brief This is the constructor for the slist. It sets the initial size 
/// of the list to zero and points the head (m_front) at NULL.
/// @pre No SList exists
/// @post An empty SList of size 0 and with m_front pointint at NULL exists
/// @param None (default constructor)
/// @return None (constructor)
//////////////////////////////////////////////////////////////////////////////
template <class generic>
SList<generic>::SList ()
{
  m_size = 0;
  m_front = NULL;
}

//////////////////////////////////////////////////////////////////////////////
/// @fn SList (const SList <generic> &)
/// @brief This is the copy constructor. It copies a list (given as a 
/// parameter) directly into the new list. (Note: this is a deep copy)
/// @pre A SList exists. 
/// @post A new SList exists, an exact copy of the list passed as a parameter.
/// @param The Slist to be copied.
/// @return None (constructor)
////////////////////////////////////////////////////////////////////////////// 
template <class generic>
SList<generic>::SList (const SList <generic> &sl)
{
  m_size = 0;
  m_front = NULL;
  
  *this = sl;
}

//////////////////////////////////////////////////////////////////////////////
/// @fn ~SList ()
/// @brief This is the destructor for the SList. It calls clear, which just
/// deletes everything in the list using pop_fronts.
/// @pre An SList exists
/// @post No SList exists (this assists in deleting it.)
/// @param None. (Destructor)
/// @return None. (Destructor)
////////////////////////////////////////////////////////////////////////////// 
template <class generic>
SList<generic>::~SList ()
{
  clear();
}

//////////////////////////////////////////////////////////////////////////////
/// @fn SList & operator= (const SList &)
/// @brief The assignment operator definition for the SList class. This puts 
/// everything in list to the right of the operator into the list on the left
/// of the operator. 
/// @pre Two linked lists exist.
/// @post Both lists now contain identical information.
/// @param The list to be set.
/// @param The list to be gotten.
/// @return The SList, such that it is identical to the other SList.
////////////////////////////////////////////////////////////////////////////// 
template <class generic>
SList<generic> & SList<generic>:: operator= (const SList <generic> &sl)
{
  if(sl.size() == 0)
  {
    clear();
  }
  else if(sl.size() == 1)
  {
    clear();//Clear the calling list.
	push_front(sl.front());
	
  }
  else//more than one
  {
	clear();//Clear the calling list.
	
	//This chunk segfaults when I run the test through it, 
	//so I'm going to comment it out and call it a night. 
	//I think I understand the concepts, but my implementation 
	//just keeps segfaulting when I try to actually run it.
	
	SListIterator<generic> j=sl.begin();//Create iterator
  
	//Starting with a pop front 
	//and then a for loop keeps the m_front from getting detached 
	//from the front of the list.
	push_front(sl.front()); //set first item
  
     
	SNode<generic>* temp1 = m_front; //temp1 begins at m_front
	SNode<generic>* temp2;
	
	
	
	
	//J begins at beginning plus one
	for( j++; j != sl.end(); j++)
	{
		temp2 = new SNode<generic>; //Make a new node
		temp2 -> forward = NULL; //Set the pointer on the new node to null
		temp2 -> data = *j; //Copy from old list into new node
		temp1 -> forward = temp2; //Link one and two
	    temp1 = temp1 -> forward;//Move forward one
		m_size++;
	}
	
  }
  
  
  return *this;
}

//////////////////////////////////////////////////////////////////////////////
/// @fn void pop_front ()
/// @brief This removes an item from the front of the list. 
/// @pre An SList contains at least one element.
/// @post The SList contains one less element, and size is decremented by one.
/// @param None.
/// @return None (void)
////////////////////////////////////////////////////////////////////////////// 
template <class generic>
void SList<generic>::pop_front ()
{
  

  if(m_front == NULL)//no items in list
  {
    
   throw Exception(CONTAINER_EMPTY, "There are no items in the list.");
   
  }
  
  SNode<generic>* temp = m_front; //create temp and set temp equal to m_front
  m_front = m_front -> forward; //Move m_front forward one.
  delete temp; //Delete the item pointed at by temp (the old m_front
  
  m_size--;//Decrement size
}

//////////////////////////////////////////////////////////////////////////////
/// @fn void push_front (generic)
/// @brief Pushes a new item onto the front of the list.
/// @pre SList exists.
/// @post SList contains one more element, and size is incremented by one.
/// @param Takes in the element to be added.
/// @return None (void)
////////////////////////////////////////////////////////////////////////////// 
template <class generic>
void SList<generic>::push_front (generic x)
{
  //If nothing in the array
  if(m_front == NULL)
  {
    m_front = new SNode<generic>; //Make a new node that m_front points to
	m_front -> data = x; //Put data in the node
	m_front -> forward = NULL; //make the pointer in that node null since there was nothing in the array previously.
	
  }
  else//Something already in the array
  {
    SNode<generic>* temp; //Create new pointer
	temp = m_front; //Point temp at m_front
	m_front = new SNode<generic>; //Create a new node with m_front
	m_front -> forward = temp; //Set the pointer on the new node to point at temp (the old m_front)
	m_front -> data = x; //Put the data in the node
  }
  m_size++;//The list has one more item in it.
  
  return;//void
}

//////////////////////////////////////////////////////////////////////////////
/// @fn void remove (generic)
/// @brief This removes one instance of the passed value from the list.
/// Note that if the passed value is not in the list, remove will throw an 
/// "ITEM_NOT_FOUND" exception.
/// @pre A linked list exists.
/// @post The linked list contains one less instance of the passed value.
/// Size is decremented accordingly.
/// @param Value to be removed
/// @return None (void)
////////////////////////////////////////////////////////////////////////////// 
template <class generic>
void SList<generic>::remove(generic x)
{
  if(empty())
  {
    throw Exception (CONTAINER_EMPTY, "The list has nothing in it.");
  }
  else if(front() == x)//x is the first item
  {
    pop_front();
  }
  else//x should be out there, but is not first item
  {
    SNode<generic>* temp1;
    SNode<generic>* temp2;
  
	temp1 = m_front;
	while(temp1 -> forward != NULL && temp1 -> forward -> data != x)
	{
	  temp1 = temp1 -> forward;
	}
	if(temp1 -> forward == NULL)//We hit the end of the list without finding the item
	{
	  throw Exception (ITEM_NOT_FOUND, "The specified element was not found.");
	}
    else//we found it
	{
	  temp2 = temp1 -> forward -> forward;
	  delete temp1 -> forward;
	  temp1 -> forward = temp2;
	  m_size--;
	  return;
	}
  }
  
  
}

//////////////////////////////////////////////////////////////////////////////
/// @fn generic front () const
/// @brief Returns the value of the first item in the list
/// @pre List with content exists.
/// @post No change. 
/// @param None.
/// @return Returns value of the first item in the list.
//////////////////////////////////////////////////////////////////////////////
template <class generic>
generic SList<generic>::front () const
{ 
  if(empty())//No items in the list
  {
    throw Exception(CONTAINER_EMPTY, "There is no data to return.");
  }
  else
  {
    return (m_front -> data); //return the data from the first node
  }
}

//////////////////////////////////////////////////////////////////////////////
/// @fn void clear ()
/// @brief Empties the list completely by continuously calling pop_front until
/// the list is empty.
/// @pre List exists
/// @post List has nothing in it, and m_front points to null.
/// @param None.
/// @return None. (void)
////////////////////////////////////////////////////////////////////////////// 
template <class generic>
void SList<generic>::clear ()
{
  while(!empty())//So long as we have something in the list
  {
    pop_front(); //remove an item
  }
}

//////////////////////////////////////////////////////////////////////////////
/// @fn unsigned int size () const
/// @brief Returns the current number of elements in the list
/// @pre List exists
/// @post No change.
/// @param None.
/// @return Returns the current number of items in the array (0 - whatever.)
////////////////////////////////////////////////////////////////////////////// 
template <class generic>
unsigned int SList<generic>::size () const
{
  return m_size;
}


//////////////////////////////////////////////////////////////////////////////
/// @fn bool empty () const
/// @brief Returns whether or not the list is empty
/// @pre List exists
/// @post No change
/// @param None
/// @return Boolean telling if the list is empty or not.
////////////////////////////////////////////////////////////////////////////// 
template <class generic>
bool SList<generic>::empty () const
{
  if(m_front == NULL)
  {
    return true;
  }
  else
  {
    return false;
  }
}

template <class generic>
SListIterator<generic> SList<generic>::begin () const
{
    return Iterator (m_front);
}

template <class generic>
SListIterator<generic> SList<generic>::end () const
{
    return Iterator ();
}
