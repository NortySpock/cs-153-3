//////////////////////////////////////////////////////////////////////////////
/// @file slist.h
/// @author David Norton :: CS153 Section 1B
/// @brief This is the header for the SList class
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @class SList
/// @brief Defines all the functions and variables of the SList class
/// (SList is shorthand for a Singly Linked List)
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn SList ()
/// @brief This is the constructor for the slist. It sets the initial size 
/// of the list to zero and points the head (m_front) at NULL.
/// @pre No SList exists
/// @post An empty SList of size 0 and with m_front pointint at NULL exists
/// @param None (default constructor)
/// @return None (constructor)
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn SList (const SList <generic> &)
/// @brief This is the copy constructor. It copies a list (given as a 
/// parameter) directly into the new list. (Note: this is a deep copy)
/// @pre A SList exists. 
/// @post A new SList exists, an exact copy of the list passed as a parameter.
/// @param The Slist to be copied.
/// @return None (constructor)
////////////////////////////////////////////////////////////////////////////// 

//////////////////////////////////////////////////////////////////////////////
/// @fn ~SList ()
/// @brief This is the destructor for the SList. It calls clear, which just
/// deletes everything in the list using pop_fronts.
/// @pre An SList exists
/// @post No SList exists (this assists in deleting it.)
/// @param None. (Destructor)
/// @return None. (Destructor)
////////////////////////////////////////////////////////////////////////////// 

//////////////////////////////////////////////////////////////////////////////
/// @fn SList & operator= (const SList &)
/// @brief The assignment operator definition for the SList class. This puts 
/// everything in list to the right of the operator into the list on the left
/// of the operator. 
/// @pre Two linked lists exist.
/// @post Both lists now contain identical information.
/// @param The list to be set.
/// @param The list to be gotten.
/// @return The SList, such that it is identical to the other SList.
////////////////////////////////////////////////////////////////////////////// 

//////////////////////////////////////////////////////////////////////////////
/// @fn void push_front (generic)
/// @brief Pushes a new item onto the front of the list.
/// @pre SList exists.
/// @post SList contains one more element, and size is incremented by one.
/// @param Takes in the element to be added.
/// @return None (void)
////////////////////////////////////////////////////////////////////////////// 

//////////////////////////////////////////////////////////////////////////////
/// @fn void pop_front ()
/// @brief This removes an item from the front of the list. 
/// @pre An SList contains at least one element.
/// @post The SList contains one less element, and size is decremented by one.
/// @param None.
/// @return None (void)
////////////////////////////////////////////////////////////////////////////// 

//////////////////////////////////////////////////////////////////////////////
/// @fn void remove (generic)
/// @brief This removes one instance of the passed value from the list.
/// If the passed value is not in the list, you will get an "ITEM_NOT_FOUND"
/// exception.
/// @pre A linked list exists.
/// @post The linked list contains one less instance of the passed value.
/// Size is decremented accordingly.
/// @param Value to be removed
/// @return None (void)
////////////////////////////////////////////////////////////////////////////// 

//////////////////////////////////////////////////////////////////////////////
/// @fn generic front () const
/// @brief Returns the value of the first item in the list
/// @pre List with content exists.
/// @post No change. 
/// @param None.
/// @return Returns value of the first item in the list.
////////////////////////////////////////////////////////////////////////////// 

//////////////////////////////////////////////////////////////////////////////
/// @fn void clear ()
/// @brief Empties the list completely by continuously calling pop_front until
/// the list is empty.
/// @pre List exists
/// @post List has nothing in it, and m_front points to null.
/// @param None.
/// @return None. (void)
////////////////////////////////////////////////////////////////////////////// 

//////////////////////////////////////////////////////////////////////////////
/// @fn unsigned int size () const
/// @brief Returns the current number of elements in the list
/// @pre List exists
/// @post No change.
/// @param None.
/// @return Returns the current number of items in the array (0 - whatever.)
////////////////////////////////////////////////////////////////////////////// 

//////////////////////////////////////////////////////////////////////////////
/// @fn bool empty () const
/// @brief Returns whether or not the list is empty
/// @pre List exists
/// @post No change
/// @param None
/// @return Boolean telling if the list is empty or not.
////////////////////////////////////////////////////////////////////////////// 

#ifndef SLIST_H
#define SLIST_H

#include "exception.h"
#include "snode.h"
#include "slistiterator.h"

template <class generic>
class SList
{
  public:
    SList ();
    SList (const SList <generic> &);
    ~SList ();
    SList & operator= (const SList &);
    void push_front (generic);
    void pop_front ();
    void remove (generic);
    generic front () const;
    void clear ();
    unsigned int size () const;
    bool empty () const;
    typedef SListIterator<generic> Iterator;
    Iterator begin () const;
    Iterator end () const;

  private:
    unsigned int m_size; //The current size of the list
    SNode<generic> * m_front; //The list
};

#endif
#include "slist.hpp"
