//////////////////////////////////////////////////////////////////////////////
/// @file slistiterator.h
/// @author Matt Buechler :: CS153 Section 1B
/// @brief This is the header file for the slistiterator class
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
/// @class SNode
/// @brief A struct that defines the nodes used in the list
/// They contain a unit of data and the pointer to the next item.
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn SListIterator ()
/// @brief Constructs the iterator
/// @pre No iterator exists
/// @post Iterator exists
/// @param None (constructor)
/// @return None (constructor)
////////////////////////////////////////////////////////////////////////////// 

//////////////////////////////////////////////////////////////////////////////
/// @fn SListIterator (SNode<generic> *)
/// @brief Copy constructor for iterator
/// @pre A previous iterator must exist.
/// @post Current iterator contains the same settings as the old iterator.
/// @param Existing iterator to be copied
/// @return None (constructor)
////////////////////////////////////////////////////////////////////////////// 

//////////////////////////////////////////////////////////////////////////////
/// @fn generic operator* () const
/// @brief Returns value of contents at iterator
/// @pre Iterator and list must exist
/// @post No change
/// @param None
/// @return Returns value of data at current position of iterator
////////////////////////////////////////////////////////////////////////////// 

//////////////////////////////////////////////////////////////////////////////
/// @fn SListIterator operator++ ();
/// @brief Moves iterator one step along list
/// @pre List exists, iterator at abitrary point along list
/// @post List exists, iterator one more step along list
/// @param None.
/// @return Nothing
////////////////////////////////////////////////////////////////////////////// 

//////////////////////////////////////////////////////////////////////////////
/// @fn SListIterator operator++ ();
/// @brief Moves iterator indicated number of steps along list
/// @pre List exists, iterator at abitrary point along list
/// @post List exists, iterator 'n' more steps along list
/// @param 'n' steps to take along list
/// @return Nothing
////////////////////////////////////////////////////////////////////////////// 

//////////////////////////////////////////////////////////////////////////////
/// @fn bool operator== (const SListIterator &) const;
/// @brief Allows you to compare the value of data under iterators
/// @pre Iterators exist
/// @post No change.
/// @param Other iterator to compare to.
/// @return Boolean
////////////////////////////////////////////////////////////////////////////// 

//////////////////////////////////////////////////////////////////////////////
/// @fn bool operator!= (const SListIterator &) const;
/// @brief Allows you to compare the value of data under iterators
/// @pre Iterators exist
/// @post No change.
/// @param Other iterator to compare to.
/// @return Boolean
////////////////////////////////////////////////////////////////////////////// 


#ifndef SLISTITERATOR_H
#define SLISTITERATOR_H

#include "snode.h"

template <class generic>
class SListIterator
{
  public:
    SListIterator ();
    SListIterator (SNode<generic> *);
    generic operator* () const;
    SListIterator operator++ ();
    SListIterator operator++ (int);
    bool operator== (const SListIterator &) const;
    bool operator!= (const SListIterator &) const;

  private:
    SNode<generic> * m_current; //Current position along list.
};

#include "slistiterator.hpp"
#endif
