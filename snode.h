//////////////////////////////////////////////////////////////////////////////
/// @file slistiterator.h
/// @author Matt Buechler :: CS153 Section 1B
/// @brief This is the header file for the snode struct
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
/// @class SNode
/// @brief A struct that defines the nodes used in the list
/// They contain a unit of data and the pointer to the next item.
//////////////////////////////////////////////////////////////////////

#ifndef SNODE_H
#define SNODE_H

template <class generic>
struct SNode
{
    SNode<generic> * forward; //The pointer to the next node
    generic data; //The data (no matter what it may be)
};

#endif

